import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { AuthService } from './auth.service';

@Injectable()
export class VideosService {
  public user: User;
  public params;
  public url: string;

  constructor(private http: Http, public authService: AuthService) {
    this.url = 'http://localhost:3000/';
    this.authService = authService;
  }

  getAuthenticate(user) {
    return this.http.post(this.url+'user/auth', {"username":user.username, "password":user.password})
        .map(response => response.json());
  }

  getLogout(){
    this.params = new URLSearchParams();
    this.params.set('sessionId', this.authService.sessionId);
    return this.http.get(this.url+'user/logout', { search: this.params })
        .map(response => response.json());
  }

  getVideo(id){
    this.params = new URLSearchParams();
    this.params.set('sessionId', this.authService.sessionId);
    this.params.append('videoId', id);
    return this.http.get(this.url+'video', { search: this.params })
        .map(response => response.json());
  }

  getVideos(skip = 0, limit = 10){
    this.params = new URLSearchParams();
    this.params.set('sessionId', this.authService.sessionId);
    this.params.append('skip', skip);
    this.params.append('limit', limit);
    return this.http.get(this.url+'videos', { search: this.params })
        .map(response => response.json());
  }

  setRating(id, rating){
    return this.http.post(this.url+'video/ratings?sessionId='+this.authService.sessionId, { "videoId":id,"rating":rating })
        .map(response => response.json());
  }
}

interface User {
  username: string;
  password: string;
}