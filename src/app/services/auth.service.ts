import { Injectable } from '@angular/core';
import { VideosService } from './videos.service';

@Injectable()
export class AuthService {
  public loggedIn: boolean;
  public sessionId: string;
  public detailedVideoId: string;
  public status: string;
  public error: boolean;
  public videos: video[];
  public video: video;
  public loadedVideos: number;

  constructor() {
    this.loggedIn = false;
    this.sessionId = "";
    this.detailedVideoId = "";
    this.status = "";
    this.error = false;
    this.videos = [];
    this.loadedVideos = 0;
  }

}

interface video {
  description: string;
  name: string;
  ratings: number[];
  url: string;
  grade: number;
}