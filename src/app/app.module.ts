import { MaterialModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from "@angular/flex-layout";

import { AuthService } from './services/auth.service';
import { VideosService } from './services/videos.service';
import { AuthComponent } from './components/auth.component';
import { GradeComponent } from './components/grade.component';
import { AppComponent } from './app.component';
import { AveragePipe } from './pipes/average.pipe';

@NgModule({
  declarations: [
    AuthComponent,
    AppComponent,
    GradeComponent,
    AveragePipe
  ],
  imports: [
    MaterialModule.forRoot(),
    NgbModule.forRoot(),
    FlexLayoutModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [AuthService, VideosService, AppComponent],
  bootstrap: [AuthComponent, AppComponent]
})
export class AppModule { }
