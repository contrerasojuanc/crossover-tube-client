import { Component, ElementRef, ViewChild, HostListener, Input, trigger, state, style, transition, animate  } from '@angular/core';
import { AuthService } from './services/auth.service';
import { VideosService } from './services/videos.service';
import { AuthComponent } from './components/auth.component';
import { GradeComponent } from './components/grade.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  entryComponents: [GradeComponent],
  animations: [
    trigger('flyInOut', [
      state('in', style({opacity: 1, transform: 'translateX(0)'})),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('1s ease-in')
      ]),
      transition('* => void', [
        animate('1s ease-out', style({
          opacity: 1,
          transform: 'translateX(100%)'
        }))
      ])
    ]),
    trigger('flyOutIn', [
      state('in', style({opacity: 1, transform: 'translateX(0)'})),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(100%)'
        }),
        animate('1s ease-in')
      ]),
      transition('* => void', [
        animate('1s ease-out', style({
          opacity: 1,
          transform: 'translateX(-100%)'
        }))
      ])
    ])
  ]
})
export class AppComponent{
  title: string;
  username: string;
  sessionId: string;
  status: string;
  elements: ElementRef[];
  element: any;
  detailedVideo: boolean;

  constructor(public videosService: VideosService, public authService: AuthService) {
    this.title = "CrossOver Tube";
    this.authService = authService;
    this.videosService = videosService;
    this.detailedVideo = false;
  }

  //Method to load videos on scroll down
  @HostListener('window:scroll', ['$event']) 
  track(event) {
    //Code from http://blog.sodhanalibrary.com/2016/10/detect-when-user-scrolls-to-bottom-of.html#.WIlvXBvhDIV
    let windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    let body = document.body, html = document.documentElement;
    let docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    let windowBottom = windowHeight + window.pageYOffset;

    //Bottom of the page
    if (windowBottom >= docHeight && this.authService.loggedIn) {
        //Lazy load from total videos shown + 4
        let skip = this.authService.videos.length;
        let limit = 4;
        this.videosService.getVideos(skip, limit)
            .subscribe(response => {
                if(response.status == 'success') {
                    this.authService.loadedVideos += response.data.length;
                    
                    //Add new videos
                    for (var i=0; i<response.data.length; i++) {
                        this.authService.videos.push(response.data[i]);
                    }
                }
            });
    }
  }

  logout(){
    this.videosService.getLogout()
        .subscribe(response => {
            this.authService.status = response.status;
            if(response.status == 'success') {
                this.authService.loggedIn = false;
                console.log("Logged out");
            }
        });
  }

  //In order to pause all video players except the one playing
  @ViewChild('videoPlayer') videoplayer: any;
  @ViewChild('videosList') videoslist: ElementRef;
  playOnly(id) {
    this.elements = this.videoslist.nativeElement.querySelectorAll('video');

    for (this.element of this.elements) {
      if(this.element.id != id)
        this.element.pause();
    }

  }

  //Detail video
  detail(id) {
    this.videosService.getVideo(id)
        .subscribe(response => {
            this.authService.status = response.status;
            if(response.status == 'success') {
                this.authService.video = response.data;
                this.detailedVideo = true;
                this.authService.detailedVideoId = response.data._id;
            } else {
                this.detailedVideo = false;
                this.authService.detailedVideoId = "";
            }
            
        });
  }

  goGallery() {
    this.detailedVideo = false;
  }

}