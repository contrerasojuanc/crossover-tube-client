import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'average'})
export class AveragePipe implements PipeTransform {
  value: number;
  grade: number;
  sum: number = 0;
  average: number = 0;

  transform(values: number[], args: string[]): any {
    if (values==undefined || values.length == 0) return 0;
    
    this.average = 0;
    this.sum = 0;
    for(this.value of values) {
       this.sum = this.sum + this.value;
    }
    this.average = this.sum / values.length;

    return this.average;
  }
}