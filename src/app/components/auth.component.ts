import { Component } from '@angular/core';
import {Md5} from 'ts-md5/dist/md5';
import { AuthService } from '../services/auth.service';
import { VideosService } from '../services/videos.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {
  username: string;
  password: string;
  in: boolean;

  constructor(public videosService: VideosService, public authService: AuthService) {
    authService.loggedIn = false;
    this.authService = authService;
    this.videosService = videosService;
    this.in = false;
  }

  login(username, password) {
    this.videosService.getAuthenticate({username:username, password:Md5.hashStr(password)})
        .subscribe(response => {
            if(response.status == 'success') {
                this.authService.loggedIn = true;
                this.authService.error = false;
                this.authService.sessionId = response.sessionId;
                this.videosService.getVideos(0,6)
                    .subscribe(response2 => {
                        if(response2.status == 'success') {
                            this.authService.loadedVideos = response2.data.lenght;
                            this.authService.videos = response2.data;
                        }
                    });
                this.in = true;
            }
            else {
                this.authService.loggedIn = false;
                this.authService.error = true;
                this.authService.sessionId = "";
                this.in = false;
            }
        });
  }
}