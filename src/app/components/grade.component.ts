import { Component, Input } from '@angular/core';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { VideosService } from '../services/videos.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'grade-component',
  templateUrl: './grade.component.html',
  styleUrls: ['./grade.component.css'],
  providers: [NgbRatingConfig]
})
export class GradeComponent {
  @Input() currentRate: number;
  @Input() videoId: string;
  @Input() readonly: boolean;
  first: boolean;

  config: NgbRatingConfig;
  constructor(config: NgbRatingConfig, private videosService: VideosService, public authService: AuthService) {
    this.config = config;
    this.videosService = videosService;
    this.authService = authService;
    this.readonly = true;
    this.videoId = "";
    this.first = false;
    // Customize default values of ratings used by this component tree
    config.max = 5;
    config.readonly = this.readonly;
  }

  rateChange(){
    if(this.first) {
        this.videosService.setRating(this.authService.detailedVideoId ,this.currentRate)
            .subscribe(response => {
                if(response.status == 'success') {
                    console.log("New rating: "+response.data.ratings);
                    this.first = false;
                    this.readonly = true;
                }
                else {
                    console.log("Error rating");
                }
            });
    } else {
        this.first = true;
        this.readonly = false;
    }
  }
}