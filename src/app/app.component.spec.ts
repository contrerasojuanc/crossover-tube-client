/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { MaterialModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AppComponent } from './app.component';
import { AuthService } from './services/auth.service';
import { VideosService } from './services/videos.service';
import { AuthComponent } from './components/auth.component';
import { GradeComponent } from './components/grade.component';
import { AppComponent } from './app.component';
import { AveragePipe } from './pipes/average.pipe';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,    
        AuthComponent,
        GradeComponent,
        AveragePipe
      ],
      imports: [
        MaterialModule.forRoot(),
        NgbModule.forRoot(),
        BrowserModule,
        FormsModule,
        HttpModule
      ],
      providers: [AuthService, VideosService, AppComponent],
      bootstrap: [AuthComponent, AppComponent]
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'CrossOver Tube'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('CrossOver Tube');
  }));

});
