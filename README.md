# CrossOver Tube

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.26.
It is based on Angular 2 and Material UI

- The Restful API is consumed by a singleton service called services/videos.services.ts, injected to ngModule. 
This service has the methods for comunicate client-server.

- It consumes A P I resources from local server running on port 3000.

- The first screen is a video list, with title, description and rating stars.

- Videos can't be play simultaneously, on new play all other are paused.

- If click on any video title, it goes to video detail, where it's shown in big mode, and allow to be rating.

- Only the current detail video playing can be rate, once.

- It uses lazy load for show more videos, when scroll reaches to bottom of the page.

## Notice

- Server app (Folder video_portal_api-master) was made by https://www.crossover.com

## Installation

- It needs to enable CORS on server side, adding to video_portal_api-master/index.js :

    //Allowing CORS
    app.all('/*', function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
      res.header("Access-Control-Allow-Methods", "GET, POST","PUT");
      next();
    });

- After, run npm install && npm start

- This client now runs on http://localhost:4200

- For login enter username = ali and password = password 

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to GitHub Pages

Run `ng github-pages:deploy` to deploy to GitHub Pages.

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## About me

My name is Juan Carlos, I'm a software engineer, always trying to keep up to date on technology information world.
I must say that this was a really enjoyable project.
Email: contrerasojuanc@gmail.com